$(".about--group .icon--edit").click(function(){
	var ytop = $(this).position().top-20;
	var xleft = $(this).position().left + $(this).width() + 25;
	$(".profile--popup").fadeIn(200,function(){$(this).focus();});
	$(".profile--popup").css({"top":ytop,"left": xleft});
	if($(this).siblings().hasClass("about--bioname")){
	var label = "NAME";
	var data = $(this).siblings("div").html();
	}else{
		var check = $(this).siblings("div").html().split("</i>&nbsp;&nbsp;");
		var data = check[1];
		if($(this).siblings().hasClass("about--bioweb")){
		var label = "WEBSITE";
		}else{
			if($(this).siblings().hasClass("about--biophone")){
			var label = "PHONE";
			}else{
				if($(this).siblings().hasClass("about--biocity")){
				var label = "CITY, STATE & ZIP";
				}
			}
		}
	}
	$(".profile--etitle").html(label);
	$(".profile--einput").val(data);
});

$('#close').click(function(){
	$(".profile--popup").fadeOut(200);
})


$(".icon--title").click(function(){
	var array = $("#about .about--group .about--bioname").html().split(" ");
	$("#fname").val(array[0]);
	$("#lname").val(array[1]);
	var web = $("#about .about--group .about--bioweb").html().split("</i>&nbsp;&nbsp;");
	$("#web").val(web[1]);
	var phone = $("#about .about--group .about--biophone").html().split("</i>&nbsp;&nbsp;");
	$("#phone").val(phone[1]);
	var city = $("#about .about--group .about--biocity").html().split("</i>&nbsp;&nbsp;");
	$("#city").val(city[1]);
	$("#about").fadeOut(200);
	$("#aboutedit").fadeIn(200);
})

$('#closeedit').click(function(){
	$("#aboutedit").fadeOut(200);
	$("#about").fadeIn(200);
})

$('.profile--menuitem').click(function(){
	$('.profile--menuitem').removeClass('profile--menuitem--active');
	$(this).addClass('profile--menuitem--active');
	if($(this).position().left <= 0){
	var move = $(this).position().left;
	$(".profile--menuitem").animate({left:0});
	}else{
		var xwidth = $(this).position().left + $(this).width();
		if($(window).width() <= xwidth){
			var move = -xwidth + $(window).width()-120;
			$(".profile--menuitem").animate({left:move});
		}
	}
	$('.profile--bottomgroup').removeClass("profile--bottomgroup--active");
	var id = "#"+($(this).html().toLowerCase()).replace(/\s/g, '');;
	$(id).addClass('profile--bottomgroup--active');
})

$("form")[0].reset();

$("input").keyup(function(){
	if($(this).is(":invalid")){
		$(this).css({"color":"#ef1313","border-bottom":"1px solid #ef1313"});
		$(this).siblings("label").css({"color":"#ef1313","top":"-20px","font-size":"13px","text-transform":"uppercase","font-weight":"600"});
	}else{
		if($(this).is(":valid")){
		$(this).css({"color":"#505050","border-bottom":"1px solid #505050"});
		$(this).siblings("label").css({"color":"#b7b8bf","top":"-20px","font-size":"13px","text-transform":"uppercase","font-weight":"600"});
		}
	}
})

$(function() {
if ( $(window).width() < 361) {
  $("body").swipe( {
    swipeRight:function(event, direction) {
    var id = $(".profile--bottomgroup--active").attr("id");
	    if(id != "about"){
		$(".profile--bottomgroup").removeClass("profile--bottomgroup--active");	
		$(".profile--menuitem").removeClass("profile--menuitem--active");
		    if(id == "option3"){
		 	$("#option2").addClass("profile--bottomgroup--active");
		 	$(".menuitem--option2").addClass("profile--menuitem--active");
		    }
		    if(id == "option2"){
		 	$("#option1").addClass("profile--bottomgroup--active");
		 	$(".menuitem--option1").addClass("profile--menuitem--active");
		    }
		    if(id == "option1"){
		 	$("#settings").addClass("profile--bottomgroup--active");
		 	$(".menuitem--settings").addClass("profile--menuitem--active");
		    }
		    if(id == "settings"){
		 	$("#about").addClass("profile--bottomgroup--active");
		 	$(".menuitem--about").addClass("profile--menuitem--active");
		    }
	    }
		if($(".profile--menuitem--active").position().left <= 0){
		var move = $(".profile--menuitem--active").position().left;
		$(".profile--menuitem").animate({left:0});
		}else{
			var xwidth = $(".profile--menuitem--active").position().left + $(".profile--menuitem--active").width();
			if($(window).width() < xwidth){
				var move = -xwidth + $(window).width()-120;
				$(".profile--menuitem").animate({left:move});
			}
		}
    },
    swipeLeft:function(event, direction) {
    var id = $(".profile--bottomgroup--active").attr("id");
	    if(id != "option3"){
		$(".profile--bottomgroup").removeClass("profile--bottomgroup--active");	
		$(".profile--menuitem").removeClass("profile--menuitem--active");
		    if(id == "option1"){
		 	$("#option2").addClass("profile--bottomgroup--active");
		 	$(".menuitem--option2").addClass("profile--menuitem--active");
		    }
		    if(id == "settings"){
		 	$("#option1").addClass("profile--bottomgroup--active");
		 	$(".menuitem--option1").addClass("profile--menuitem--active");
		    }
		    if(id == "about"){
		 	$("#settings").addClass("profile--bottomgroup--active");
		 	$(".menuitem--settings").addClass("profile--menuitem--active");
		    }
		    if(id == "option2"){
		 	$("#option3").addClass("profile--bottomgroup--active");
		 	$(".menuitem--option3").addClass("profile--menuitem--active");
		    }
	    }
		if($(".profile--menuitem--active").position().left <= 0){
		var move = $(".profile--menuitem--active").position().left;
		$(".profile--menuitem").animate({left:0});
		}else{
			var xwidth = $(".profile--menuitem--active").position().left + $(".profile--menuitem--active").width();
			if($(window).width() < xwidth){
				var move = -xwidth + $(window).width()-120;
				$(".profile--menuitem").animate({left:move});
			}
		}
    },
    excludedElements: "label, button, input, select, textarea, .noSwipe",
  });
}
});